#!/usr/bin/python3.6

import os
import sys

os.chdir(sys.argv[1] + '/Answers')
for x in os.listdir():
	if x.endswith('.c'):
		os.system(f"i686-w64-mingw32-gcc -o {x[:-2] + '.exe'} {x}")
	elif x.isdigit() or x.endswith('.o') or x.endswith('.out'):
		os.remove(x)
