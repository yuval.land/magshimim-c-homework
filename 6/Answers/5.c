#include <stdio.h>

int main(void)
{
    int a = 0, b = 0, gcd = 0;
    int i = 0;

    printf("Please enter the first number: ");
    scanf("%d", &a);
    getchar();

    printf("Please enter the second number: ");
    scanf("%d", &b);
    getchar();

    // We know that a * b is a common divider, but we want to find a smaller one.
    for (i = 1; i <= a * b; i++)
    {
        // If the current number divides both a and b.
        if (i % a == 0 && i % b == 0)
        {
            gcd = i;
            break;
        }
    }

    printf("The greatest common divider is %d!\n", gcd);

    return 0;
}
