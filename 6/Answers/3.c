#include <stdio.h>
#include <stdbool.h>

int main(void)
{
    int number = 0, copy = 0, current = 0, sum = 0;
    int i = 0;
    bool hasFound = 0;

    printf("Please enter a number: ");
    scanf("%d", &number);

    // We don't care if a digit is 0
    for (i = 1; i < 10; i++)
    {
        // We want to change `copy` each time so we set it back to `number`
        copy = number;

        // If the current digit is found
        hasFound = false;

        // We search for the current digit inside the copy number
        while (copy > 0)
        {
            current = copy % 10;
            copy /= 10;
            // If we have found the current digit
            if (current == i)
            {
                hasFound = true;
                // We don't need to continue searching after finding the digit,
                break; // Not allowed but... eh
            }
        }

        // If we found the digit in the number, add it to the total sum.
        if (hasFound)
        {
            sum += i;
        }
    }

    printf("The sum is %d!\n", sum);

    return 0;
}
