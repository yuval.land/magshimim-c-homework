#include <stdio.h>

int main(void)
{
    int number = 0, sum = 0;
    int i = 0;

    printf("Please enter a number: ");
    scanf("%d", &number);

    for (i = 1; i < number; i++)
    {
        // if `number` is divisible by `i`, add it to the total sum
        if (number % i == 0)
        {
            sum += i;
        }
    }

    if (number == sum)
    {
        printf("%d is a perfect number!\n", number);
    }
    else
    {
        printf("%d is not perfect number!\n", number);
    }

    // Bonus:
    // %s is used to print a string!
    // printf("%d is %s a perfect number!\n", n, n == sum ? "\b" : "not");

    return 0;
}
