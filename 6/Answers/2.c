#include <stdio.h>

int main(void)
{
    // first and second are initialized to -1000
    // to signal they don't have a value yet.
    int first = -1000, second = -1000, current = 0;

    while (current != -999)
    {
        printf("Please enter a number (-999 to stop): ");
        scanf("%d%c", &current);

        if (current != -999)
        {
            if (current > first)
            {
                second = first;
                first = current;
            }
            else if (current > second)
            {
                second = current;
            }
        }
    }

    printf("First max is %d and the second is %d\n", first, second);

    return 0;
}
