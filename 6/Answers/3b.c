#include <stdio.h>
#include <stdbool.h>

int main(void)
{
    int number = 0;
    short sum = 0, digit = 0, hasFound = 0;

    printf("Please enter a number: ");
    scanf("%d", &number);
    
    while (number > 0)
    {
        digit = number % 10;

        if (!(hasFound >> digit & 1))
        {
            hasFound |= 1 << digit;
            sum += digit;
        }

        number /= 10;
    }

    printf("The sum is %d!\n", sum);

    return 0;
}
