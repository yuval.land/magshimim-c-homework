#include <stdio.h>

#define PROGRAMMING_JOKE 0
#define LOGIC_JOKE 1
#define NETWORKING_JOKE 2

void printJoke(int joke);
int userChoice();

int main(void)
{
    int choice = userChoice();

    printJoke(choice);

    return 0;
}

/*
? Prints a given joke
< Input: {int} joke - The type of joke to print
! Output: none
*/
void printJoke(int joke)
{
    switch (joke)
    {
    case PROGRAMMING_JOKE:
        printf("What do computers and air conditioners have in common?\n");
        printf("They are both useless once you open Windows.\n");
        break;

    case LOGIC_JOKE:
        printf("A programmer puts two glasses on his bedside table before going to sleep.\n");
        printf("A full one, in case he gets thirsty, and an empty one, in case he doesn’t.\n");
        break;

    case NETWORKING_JOKE:
        printf("What's the worst part about a UDP joke?\n");
        printf("You may not get it\n");
        break;

    default:
        printf("There has been a problem... please try again!\n");
        break;
    }
}

/*
? Prints the menu and gets the choice from the user
! Input: none
> Output: {int} choice - The type of joke to print
*/
int userChoice()
{
    int choice = 0;

    printf("Welcome to the Comedy Store!\n");
    printf("What kind of a joke would you like?\n");
    printf("Programming joke - 0\n");
    printf("Logic joke - 1\n");
    printf("Networking joke - 2\n");

    scanf("%d", &choice);

    return choice;
}
