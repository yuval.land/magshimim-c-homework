#include <stdio.h>
#include <math.h>

#define PI 3.14159265358979323846

enum options
{
    OPTION_DISTANCE = 1,
    OPTION_HYPOTENUSE,
    OPTION_CIRCLE,
    OPTION_RECTANGLE,
    OPTION_SQUARE,
    OPTION_EXIT
};

// Menu Function
int printMenu();

// Calculation functions
float calculateDistance(int x1, int y1, int x2, int y2);
float calculateHypotenuse(int side1, int side2);
void calculateCircleInfo(int radius);
int calculateRectArea(int width, int height);
int calculateSquareArea(int side);

int main(void)
{
    int option = 0;
    // Option #1 variables
    int x1 = 0, y1 = 0, x2 = 0, y2 = 0;
    float distance = 0;
    // Option #2 variables
    int side1 = 0, side2 = 0;
    float hypotenuse = 0;
    // Option #3 variables
    int radius = 0;
    // Option #4 variables
    int width = 0, height = 0;
    int area; // Also used in Option #5
    // Option #5 variables
    int side = 0;

    printf("Welcome to my calculator!\n");

    do
    {
        option = printMenu();

        switch (option)
        {
        case OPTION_DISTANCE:
            printf("Enter point #1 coordinates: ");
            scanf("%d %d", &x1, &y1);
            getchar();
            printf("Enter point #2 coordinates: ");
            scanf("%d %d", &x2, &y2);
            getchar();

            distance = calculateDistance(x1, y1, x2, y2);
            printf("The distance is %.2f\n", distance);

            break;

        case OPTION_HYPOTENUSE:
            printf("Enter the 2 sides of the triangle: ");
            scanf("%d %d", &side1, &side2);
            getchar();

            hypotenuse = calculateHypotenuse(side1, side2);
            printf("The hypotenuse is %.2f\n", hypotenuse);

            break;

        case OPTION_CIRCLE:
            printf("Enter radius: ");
            scanf("%d", &radius);
            getchar();

            calculateCircleInfo(radius);

            break;

        case OPTION_RECTANGLE:
            printf("Enter rectangle width: ");
            scanf("%d", &width);
            getchar();
            printf("Enter rectangle height: ");
            scanf("%d", &height);
            getchar();

            area = calculateRectArea(width, height);
            printf("The area of the rectangle is %d\n", area);

            break;

        case OPTION_SQUARE:
            printf("Enter the length of the side of the square: ");
            scanf("%d", &side);
            getchar();

            area = calculateSquareArea(side);
            printf("The area of the rectangle is %d\n", area);

            break;

        case OPTION_EXIT:
            printf("Goodbye!\n");
            break;

        default:
            printf("Wrong option!\n");
            break;
        }
    } while (option != 6);

    return 0;
}

/*
? Prints the menu and gets an option from the user.
! Input: none
> Output: {int} option - the option the user chose
*/
int printMenu()
{
    int option = 0;

    printf("\nPlease choose an option: \n");
    printf("1) Calculate the distance between 2 points\n");
    printf("2) Calculate hypotenuse of a triangle\n");
    printf("3) Calculate the area and diameter of a circle\n");
    printf("4) Calculate the area of a rectangle\n");
    printf("5) Calculate the area of a square\n");
    printf("6) Exit the prorgam\n");
    printf(">>> ");

    scanf("%d", &option);

    return option;
}

/*
? Calculates the distance between 2 given points.
< Input: {int} x1 - the X coordinate of the first point
< Input: {int} y1 - the Y coordinate of the first point
< Input: {int} x2 - the X coordinate of the second point
< Input: {int} y2 - the Y coordinate of the second point
> Output: {float} distance - the distance between the 2 points
*/
float calculateDistance(int x1, int y1, int x2, int y2)
{
    /*
    How does it work?
    The distance between 2 points is the same as the hypotenuse
    of the triangle that is created between the two points.

             * (x2, y2)
            /|
           / |
          /  |
         /   |
        /    |
       /     |
      /      |
     /       |
    /________|
    *
    (x1, y1)
    
    */
    int side1 = x2 - x1;
    int side2 = y2 - y1;
    float hypotenuse = calculateHypotenuse(side1, side2);

    return hypotenuse;
}

/*
? Calculates the hypotenuse of a triangle
< Input: {int} side1 - the first side of the triangle
< Input: {int} side2 - the second side of the triangle
> Output: {int} hypotenuse - the calculated hypotenuse of the triangle
*/
float calculateHypotenuse(int side1, int side2)
{
    // a^2 + b^2 = c^2
    float side1_squared = side1 * side1;
    float side2_squared = side2 * side2;
    float hypotenuse = sqrt(side1_squared + side2_squared);

    return hypotenuse;
}

/*
? Calculates a circle's diameter and area and prints it
* Input: {int} radius - the radius of the circle
! Output: none
*/
void calculateCircleInfo(int radius)
{
    float perimiter = 2 * radius * PI;
    float area = radius * radius * PI;

    printf("Perimiter: %.2f\n", perimiter);
    printf("Area: %.2f\n", area);
}

/*
? Calulates the area of a rectangle by it sides
< Input: {int} width - the size of the first side of the rectangle
< Input: {int} height - the size of the second side of the rectangle
> Output: {int} area - the area of the rectangle
*/
int calculateRectArea(int width, int height)
{
    int area = width * height;
    return area;
}

/*
? Calulates the area of a square by it side
< Input: {int} side - the size of the side of the square
> Output: {int} area - the area of the square
*/
int calculateSquareArea(int side)
{
    int area = calculateRectArea(side, side);
    return area;
}
