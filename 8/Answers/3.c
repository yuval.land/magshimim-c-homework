#include <stdio.h>

/*
B) What are the advantages of using `enum`?
We can group multiple constants together,
and we don't need to give a value for each variable.

C) How can we make the enum start from 1 and not from 0?
We give the first variable a value of 1,
so that the next variable will have a value of 2 and so on.

D) 2 more examples of enum uses
enum daysOfWeek
{
    SUNDAY = 1,
    MONDAY,
    TUESDAY,
    WEDNESDAY,
    THURSDAY,
    FRIDAY,
    SATURDAY
};

enum months
{
    JANUARY = 1,
    FEBRUARY,
    MARCH,
    APRIL,
    MAY,
    JUNE,
    JULY,
    AUGUST,
    SEPTEMBER,
    OCTOBER,
    NOVEMBER,
    DECEMBER
};
*/

enum difficultyLevel
{
    EASY = 1,
    MEDIUM,
    HARD,
    EXTREME
};

int main(void)
{
    int level = 0;

    printf("Please choose a difficulty level:\n");
    printf("1) Easy\n");
    printf("2) Medium\n");
    printf("3) Hard\n");
    printf("4) Extreme\n");

    scanf("%d", &level);

    switch (level)
    {
    case EASY:
        printf("You chose EASY... I wasn't expecting much anyway...\n");
        break;

    case MEDIUM:
        printf("You chose MEDIUM. That'll be fun.\n");
        break;

    case HARD:
        printf("You chose HARD! Now we're talking!\n");
        break;

    case EXTREME:
        printf("You chose EXTREME!! Good luck! You will need it!\n");
        break;

    default:
        printf("You chose a wrong option...\n");
        break;
    }

    return 0;
}
