/*********************************
* Class: MAGSHIMIM C1			 *
* Week 8           				 *
**********************************/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define ARE_VALID 1
#define ARE_INVALID 0

int areValidNumbers(int num1, int num2, int num3);
int isAnyEven(int num1, int num2, int num3);
int isAnyOdd(int num1, int num2, int num3);
int isAnyOver50(int num1, int num2, int num3);

int main(void)
{
    int num1 = 0;
    int num2 = 0;
    int num3 = 0;
    int areValid = 0;

    srand(time(0));

    do
    {
        num1 = rand() % 100;
        num2 = rand() % 100;
        num3 = rand() % 100;

        areValid = areValidNumbers(num1, num2, num3);
    } while (areValid == ARE_INVALID);

    printf("The numbers are %d, %d and %d!\n", num1, num2, num3);

    return 0;
}

/*
? Checks if the 3 numbers follow the rules
< Input: {int} num1, num2, num3 - the numbers to check
> Output: {int} ans - are the numbers valid or not
*/
int areValidNumbers(int num1, int num2, int num3)
{
    int ans = 0;

    if (isAnyEven(num1, num2, num3) && isAnyOdd(num1, num2, num3) && isAnyOver50(num1, num2, num3))
    {
        ans = ARE_VALID;
    }
    else
    {
        ans = ARE_INVALID;
    }

    return ans;
}

/*
? Checks if any of the numbers are even
< Input: {int} num1, num2, num3 - the numbers to check
> Output: {int} ans - is any of the numbers even
*/
int isAnyEven(int num1, int num2, int num3)
{
    int ans = 0;

    if (num1 % 2 == 0 || num2 % 2 == 0 || num3 % 2 == 0)
    {
        ans = ARE_VALID;
    }
    else
    {
        ans = ARE_INVALID;
    }

    return ans;
}

/*
? Checks if any of the numbers are odd
< Input: {int} num1, num2, num3 - the numbers to check
> Output: {int} ans - is any of the numbers odd
*/
int isAnyOdd(int num1, int num2, int num3)
{
    int ans = 0;

    if (num1 % 2 == 1 || num2 % 2 == 1 || num3 % 2 == 1)
    {
        ans = ARE_VALID;
    }
    else
    {
        ans = ARE_INVALID;
    }

    return ans;
}

/*
? Checks if any of the numbers are bigger than 50
< Input: {int} num1, num2, num3 - the numbers to check
> Output: {int} ans - is any of the numbers bigger than 50
*/
int isAnyOver50(int num1, int num2, int num3)
{
    int ans = 0;

    // This 50 IS a magic number
    if (num1 > 50 || num2 > 50 || num3 > 50)
    {
        ans = ARE_VALID;
    }
    else
    {
        ans = ARE_INVALID;
    }

    return ans;
}
