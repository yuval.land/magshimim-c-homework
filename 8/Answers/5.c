#include <stdio.h>

int getDaysInMonth(int month);
int checkInput(int day, int month);
int getCurrentWeekday(int firstDay, int currentDay);

#define MAX_MONTHS 12
#define INPUT_VALID 1
#define INPUT_INVALID 0

enum daysOfWeek
{
    SUNDAY = 1,
    MONDAY,
    TUESDAY,
    WEDNESDAY,
    THURSDAY,
    FRIDAY,
    SATURDAY
};

enum months
{
    JANUARY = 1,
    FEBRUARY,
    MARCH,
    APRIL,
    MAY,
    JUNE,
    JULY,
    AUGUST,
    SEPTEMBER,
    OCTOBER,
    NOVEMBER,
    DECEMBER
};

int main(void)
{
    int day = 0, month = 0, firstDay = 0, weekday = 0, isValid = 0;

    printf("Hello! Welcome to the day calculator!\n");

    do
    {
        printf("Enter month to check: (1-Jan, 2-Feb, etc) ");
        scanf("%d", &month);
        getchar();
        printf("Enter day to check: ");
        scanf("%d", &day);
        getchar();

        isValid = checkInput(day, month);

        if (isValid == INPUT_INVALID)
        {
            printf("Invalid input, try again\n");
        }
    } while (isValid == INPUT_INVALID);

    printf("Enter the weekday of the 1st day of the month: (1-Sunday, 2-Monday, etc) ");
    scanf("%d", &firstDay);
    getchar();

    printf("The %d.%d will be a ", day, month);
    weekday = getCurrentWeekday(firstDay, day);
    switch (weekday)
    {
    case SUNDAY:
        printf("Sunday\n");
        break;
    
    case MONDAY:
        printf("Monday\n");
        break;
    
    case TUESDAY:
        printf("Tuesday\n");
        break;
    
    case WEDNESDAY:
        printf("Wednesday\n");
        break;
    
    case THURSDAY:
        printf("Thursday\n");
        break;
    
    case FRIDAY:
        printf("Friday\n");
        break;
    
    case SATURDAY:
        printf("Saturday\n");
        break;
    
    default:
        printf("*ERROR*");
        break;
    }

    return 0;
}

/*
? Returns the maximum number of days in a month.
< Input: {int} month - the month to check
> Output: {int} maxDays - the max number of days in the month
*/
int getDaysInMonth(int month)
{
    int maxDays = 0;

    switch (month)
    {
    case JANUARY:
    case MARCH:
    case MAY:
    case JULY:
    case AUGUST:
    case OCTOBER:
    case DECEMBER:
        maxDays = 31;
        break;

    case APRIL:
    case JUNE:
    case SEPTEMBER:
    case NOVEMBER:
        maxDays = 30;
        break;

    case FEBRUARY:
        maxDays = 28;
        break;

    default:
        maxDays = -1;
    }

    return maxDays;
}

/*
? Checks if a given date is valid
< Input: {int} day - the day of the date to check
< Input: {int} month - the month of the date to check
< Input: {int} year - the year of the date to check
> Output: {int} isValid - is the given date valid
*/
int checkInput(int day, int month)
{
    int isValid = 0, daysInMonth = getDaysInMonth(month);

    // If the input is valid
    if (day > 0 && day <= daysInMonth && month > 0 && month <= MAX_MONTHS)
    {
        isValid = INPUT_VALID;
    }
    else
    {
        isValid = INPUT_INVALID;
    }

    return isValid;
}

/*
? Checks the weekday of the current day
< Input: {int} firstDay - the first day in the month
< Input: {int} currentDay - the current day in the month
> Output: {int} weekday - the weekday of the current day
*/
int getCurrentWeekday(int firstDay, int currentDay)
{
    // The reason we remove 2 is because we want the difference between them (-1)
    // and we already counted firstDay (-1).
    // We add one bcause %7 returns 0 to 6 and we need 1 to 7.
    int weekday = (currentDay + firstDay - 2) % 7 + 1;
    return weekday;
}

