#include <stdio.h>
#include <stdlib.h> // rand()

int roll(int sides);

int main(void)
{
    int sides = 0, rolledNumber = 0;

    printf("Enter the number of sides: ");
    scanf("%d", &sides);
    getchar();
    rolledNumber = roll(sides);

    printf("The dice rolled %d!\n", rolledNumber);

    // Lets test this code:
    // for (int i = 20; i > 0; i--)
    // {
    //     getchar();

    //     rolledNumber = roll(sides);
    //     printf("The dice rolled %d! {%d left}\n", rolledNumber, i);
    // }

    return 0;
}

/*
? Generates a random dice number.
< Input: {int} sides - number of sides the dice has
> Output: {int} number - generated number between 1 and sides (inclusive)
*/
int roll(int sides)
{
    // The reason we add 1 is we'll get numbers between 0 and sides-1
    int number = rand() % sides + 1;
    return number;
}
