#include <stdio.h>
#include <string.h>
#include <ctype.h> // to lowercase

#define SIZE 100

#define TRUE 1
#define FALSE 0

// Bonus:
char toLowercase(char c);
int isPalindrome(char s[]);

int main(void)
{
    char string[SIZE] = {0};

    printf("Enter the string to check: ");
    fgets(string, SIZE, stdin);

    if (string[strlen(string) - 1] == '\n')
    {
        string[strlen(string) - 1] = 0;
    }

    printf("Is the string a palindrome: ");
    if (isPalindrome(string))
    {
        printf("Yes\n");
    }
    else
    {
        printf("No\n");
    }

    return 0;
}

/**
 * ? Checks if a given string is a palindrome or not
 * > Input: {char[]} string - the string to check
 * < Output: {int} palindrome - is the string a palindrome or not
 */
int isPalindrome(char string[])
{
    int palindrome = TRUE;
    int left = 0, right = strlen(string) - 1;

    while (left < right && palindrome)
    {
        if (string[left] == ' ')
        {
            ++left;
        }
        else if (string[right] == ' ')
        {
            --right;
        }
        else
        {
            if (tolower(string[left]) != tolower(string[right]))
            {
                palindrome = FALSE;
            }
            ++left;
            --right;
        }
    }

    return palindrome;
}
