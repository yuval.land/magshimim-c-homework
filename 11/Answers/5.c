#include <stdio.h>
#include <string.h>
#include <ctype.h>

#define TRUE 1
#define FALSE 0

#define SIZE 100

#define MAXIMUM_LENGTH 8
#define MINIMUM_LENGTH 6

int main(void)
{
    char password[SIZE] = {0};
    int hasDigit = FALSE, hasUppercase = FALSE, hasLowercase = FALSE;
    int isRepeating = FALSE, isCorrectLength = TRUE;

    printf("Enter a password: ");
    fgets(password, SIZE, stdin);

    if (password[strlen(password) - 1] == '\n')
    {
        password[strlen(password) - 1] = 0;
    }

    if (strlen(password) < MINIMUM_LENGTH || strlen(password) > MAXIMUM_LENGTH)
    {
        isCorrectLength = FALSE;
    }

    for (int i = 0; password[i] && !isRepeating && isCorrectLength; i++)
    {
        if (isdigit(password[i]))
        {
            hasDigit = TRUE;
        }
        else if (islower(password[i]))
        {
            hasLowercase = TRUE;
        }
        else if (isupper(password[i]))
        {
            hasUppercase = TRUE;
        }

        if (password[i] == password[i + 1])
        {
            isRepeating = TRUE;
        }
    }

    printf("Is the password valid: ");
    if (isCorrectLength && hasDigit && hasLowercase && hasUppercase && !isRepeating)
    {
        printf("Yes\n");
    }
    else
    {
        printf("No\n");
    }

    return 0;
}