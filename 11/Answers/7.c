#include <stdio.h>
#include <string.h>

#define SIZE 100
#define ALPHABET_SIZE 26


int main(void)
{
    char string[SIZE] = {0};
    char occurences[ALPHABET_SIZE] = {0};
    char commonCharFirst = 0, commonCharSecond = 0;
    int commonCountFirst = 0, commonCountSecond = 0;

    printf("Enter a string: ");
    fgets(string, SIZE, stdin);

    // Removing the final newline if it's necessary
    if (string[strlen(string) - 1] == '\n')
    {
        string[strlen(string) - 1] = 0;
    }

    // Getting the occurences of each letter
    for (int i = 0; string[i]; i++)
    {
        // Ignore spaces
        if (string[i] != ' ')
        {
            // a = 0, b = 1, c = 2 ... z = 25
            ++occurences[string[i] - 'a'];
        }
    }

    // Finding the most common and the second most common
    // We need the value of the most common AND it's letter!
    for (int i = 0; i < ALPHABET_SIZE; i++)
    {
        // If we found a new most common letter
        if (occurences[i] > commonCountFirst)
        {
            // second = first
            commonCountSecond = commonCountFirst;
            commonCharSecond = commonCharFirst;

            // first = current
            commonCountFirst = occurences[i];
            commonCharFirst = 'a' + i;
        }
        // If we found a new second most common letter
        else if (occurences[i] > commonCountSecond)
        {
            // second = current
            commonCountSecond = occurences[i];
            commonCharSecond = 'a' + i;
        }
    }

    printf("The most common character is %c and the second most is %c!\n", commonCharFirst, commonCharSecond);

    // Swapping the most common letters
    for (int i = 0; string[i]; i++)
    {
        // If the character is the most common
        // replace it with the second most common
        if (string[i] == commonCharFirst)
        {
             string[i] = commonCharSecond;
        }
        // If the character is the second most common
        // replace it with the most common
        else if (string[i] == commonCharSecond)
        {
            string[i] = commonCharFirst;
        }
    }

    printf("After swapping: %s\n", string);

    return 0;
}
