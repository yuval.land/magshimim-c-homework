#include <stdio.h>
#include <string.h>

#define SIZE 10

int main(void)
{
    char name[SIZE] = {0};
    printf("Please enter your name: ");

    /**
     * What happens is that `fgets` also inserts a new line at the end,
     * unless the newline is out of the string's space
     */
    fgets(name, SIZE, stdin);
    // If way, if the length of the string is smaller that the size.
    // We leave an empty spot for the new line when we check (`SIZE - 1` and not `SIZE`)
    if (strlen(name) < SIZE - 1)
    {
        name[strlen(name) - 1] = 0;
    }
    // Second way, if the final character is a newline, replace it with `0`
    if (name[strlen(name) - 1] == '\n')
    {
        name[strlen(name) - 1] = 0;
    }

    printf("Your name is %s and it is %d letters long.\n", name, strlen(name));

    return 0;
}