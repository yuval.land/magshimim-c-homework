#include <stdio.h>
#include <math.h>

int main(void)
{
    int a = 0, b = 0, c = 0;
    float x1 = 0, x2 = 0, determinant = 0;

    printf("Enter the equation: ");
    // Is this cheating?
    scanf("%dx2%dx%d", &a, &b, &c);

    determinant = pow(b, 2) + 4 * a * c;
    if (determinant < 0)
    {
        printf("No solutions found!");
    }
    else if (determinant > 0)
    {
        x1 = (-b + sqrt(determinant)) / (2 * a);
        x2 = (-b - sqrt(determinant)) / (2 * a);
        printf("Solutions are %.2f and %.2f!", x1, x2);
    }
    else
    {
        x1 = (-b) / (2 * a);
        printf("Solution is %.2f!", x1);
    }

    return 0;
}