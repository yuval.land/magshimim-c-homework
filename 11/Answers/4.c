#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define SIZE 12

void shiftArray(char string[], int size);
char toChar(int n);

int main(void)
{
    char string[SIZE] = {0}, sign = 0;
    int number = 0;

    printf("Enter a number: ");
    scanf("%d", &number);

    if (number > 0)
    {
        sign = '+';
    }
    else if (number < 0)
    {
        sign = '-';
    }
    else
    {
        sign = '0';
    }

    number = abs(number);

    while (number != 0)
    {
        string[0] = toChar(number % 10);
        number /= 10;
        shiftArray(string, SIZE);
    }
    string[0] = sign;

    printf("The string is: %s, it's length is %ld\n", string, strlen(string));


    return 0;
}

/**
 * ? Shifts an array to the right and inserts a 0 at the start.
 * ? {1, 5, 3, 8} -> {0, 1, 5, 3}
 * > Input: {char[]} string - the array to shift
 * > Input: {int} size - the size of the array
 * ! Output: none
 */
void shiftArray(char string[], int size)
{
    char previous = 0;

    // We can't move the final value
    for (int i = size - 1; i > 0; i--)
    {
        string[i] = string[i - 1];
    }

    string[0] = 0;
}

/**
 * ? Converts an int to a character
 * > Input: {int} n - the number to convert
 * < Output: {char} - the character representation of the number
 */
char toChar(int n)
{
    return n + '0';
}
