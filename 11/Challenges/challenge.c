// String Challenge

#include <stdio.h>
#include <string.h>

int main(void)
{
    // Just 2 normal strigs... right?
    char secret[] = "Secret String";
    char string[] = "Normal String";
    
    // Here, see for yourselves!
    printf("string: %s, size: %ld, location: %p\n", string, strlen(string), &string);
    printf("secret: %s, size: %ld, location: %p\n", secret, strlen(secret), &secret);

    // Setting the new value of string.
    printf("Enter the new string: ");
    scanf("%s", string);
    
    // Should stay the same, right?
    printf("string: %s, size: %ld\n", string, strlen(string));
    printf("secret: %s, size: %ld\n", secret, strlen(secret));

    // How can you change the secret without changing the string?
    if (!strcmp(secret, "password"))
    {
        printf("You won!\n");
    }

    return 0;
}
