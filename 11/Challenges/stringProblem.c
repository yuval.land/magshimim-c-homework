#include <stdio.h>

// Change start between -2 and 7, and END between 5 and 9
#define START 0
#define END 7

char c1 = '1';
char c2 = '2';
char s[7] = {'s', 't', 'r', 0, 'n', 'g', 0};
char c3 = '3';
char c4 = '4';

void printVariables()
{
    printf("c1: \t %p \t %c \n", &c1, c1);
    printf("c2: \t %p \t %c \n", &c2, c2);

    for (int i = START; i < END; i++)
        printf("s[%d]: \t %p \t %c \n", i, &s[i], s[i]);
    
    printf("c3: \t %p \t %c \n", &c3, c3);
    printf("c4: \t %p \t %c \n", &c4, c4);

    printf("s: \t %p \t %s \n", &s, s);
    printf("\n");

}

int main(void)
{
    printVariables();

    // Your mission is to change c3 and c4!
    printf("\nEnter the string: ");
    scanf("%s", s);
    printf("\n\n");

    printVariables();

    return 0;
}
