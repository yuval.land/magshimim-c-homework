/*********************************
* Class: MAGSHIMIM C1			 *
* Week 11           			 *
* Class example   				 *
**********************************/

#include <stdlib.h>
#include <stdio.h>

	
int main(void)
{
	char name[] = "Uriel";
	char anotherName[] = {'U', 'r', 'i', 'e', 'l', 0};
	
	if(name == anotherName)
	{
		printf("Same Same!");
	}
	else{
		printf("Whaaaaaaat??");
	}
	return 0;
}
