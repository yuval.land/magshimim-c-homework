/*********************************
* Class: MAGSHIMIM C1			 *
* Week 11           			 *
* Class example   				 *
* puts  			    		 *
**********************************/

#include <stdlib.h>
#include <stdio.h>

#define STR_LEN 7

int main(void)
{
	char str[] = "Hoopla";
		
	printf("Printing the string using puts:\n");
	puts(str);
		
	return 0;
}
