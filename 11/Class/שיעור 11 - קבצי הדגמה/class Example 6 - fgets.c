/*********************************
* Class: MAGSHIMIM C1			 *
* Week 11           			 *
* Class example   				 *
* fgets  					     *
**********************************/

#include <stdlib.h>
#include <stdio.h>

#define STR_LEN 7
	
int main(void)
{
	char str[ STR_LEN ] = { 0 };
	
	printf("Please enter a string, max length - %d chars:\n", STR_LEN - 1);
	fgets(str, STR_LEN, stdin);

	puts(str);

	return 0;
}

