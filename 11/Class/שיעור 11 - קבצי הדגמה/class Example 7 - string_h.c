/*********************************
* Class: MAGSHIMIM C1			 *
* Week 11           			 *
* Class example   				 *
* string.h  				     *
**********************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define STR_LEN 51

void strlenExample(void);
void strncpyExample(void);
void strcmpExample(void);
void strncatExample(void);

int main(void)
{
	strlenExample();
		
	// strncpyExample();

	// strcmpExample();

	// strncatExample();
		
	return 0;
}

/*
Example of strlen
input: none
output: none
*/
void strlenExample(void)
{
	char name[STR_LEN] = "Arik";
	int len = strlen(name);
	
	printf("name is %s\n", name);
	printf("strlen(name) = %d\n", len);
	printf("\n\n");
}

/*
Example of strncpy
input: none
output: none
*/
void strncpyExample(void)
{
	char dest[STR_LEN] = { 0 };
	char src[STR_LEN] = "Love is in the Air";
	
	printf("src: %s \n", src);
	
	printf("Using strncpy to copy this phrase from src to dest...\n");
	strncpy(dest, src, STR_LEN); // There is room in dest for STR_LEN chars
	
	printf("dest: %s \n", dest);
	printf("\n\n");
}

/*
Example of strcmp
input: none
output: none
*/
void strcmpExample(void)
{
	char str1[STR_LEN] = "ABC"; // change it for tests
	char str2[STR_LEN] = "ZZZ";
	int res = 0;
	
	printf("str1: %s \nstr2: %s \n", str1, str2);
	
	printf("Using strcmp to compare the strings (alphatbetically)...\n");
	
	res = strcmp(str1, str2);    
	if (res == 0)
	{
		printf("Strings are the same\n");
	}
	else if (res > 0)
	{
		printf("str1 is bigger (later in the dictionary)\n");
	}
	else
	{
		printf("str1 is smaller (before in the dictionary)\n");
	}
	printf("\n\n");
}

/*
Example of strncat
input: none
output: none
*/
void strncatExample(void)
{
	char name[STR_LEN] = "Arik";
	printf("Name: %s\n", name);
	printf("Using strncat to add a last name...result:\n");
	
	strncat(name, " Einstein", 9); 
	//name will contain “Arik Einstein”
	
	puts (name);
	
	printf("\n\n");
}