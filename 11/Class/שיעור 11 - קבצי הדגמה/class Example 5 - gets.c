/*********************************
* Class: MAGSHIMIM C1			 *
* Week 11           			 *
* Class example   				 *
* gets	  			     *
**********************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define STR_LEN 7

int main(void)
{
	
	char str[STR_LEN] = { 0 };
	
	printf("Please enter new string: (max - %d chars): ", STR_LEN - 1);
	gets(str);
	puts(str);
	
	return 0;
}
