/*********************************
* Class: MAGSHIMIM C1			 *
* Week 11           			 *
* Class example   				 *
* scanf printf with strings	     *
**********************************/

#include <stdlib.h>
#include <stdio.h>

#define STR_LEN 7
	
int main(void)
{
	char str[STR_LEN] = "Roei";
	
	printf("The string: %s\n\n", str);

	printf("Please enter new string: (max - %d chars)\n", STR_LEN - 1);
	scanf("%s", str);
	printf("%s", str);
	printf("\n");

	return 0;
}
