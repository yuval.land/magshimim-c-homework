/*********************************
* Class: MAGSHIMIM C1			 *
* Week 11          				 *
**********************************/
#include <stdlib.h>
#include <stdio.h>

#define SIZE 5
#define ITERATIONS 1000 //later change to 5
int main(void)
{
	int i = 0;
	char str[SIZE] = {0};
	
	for(i = 0; i < ITERATIONS; i++)
	{
		printf("%d\n", i);
		str[i] = 'a';
	}

	return 0;
}
