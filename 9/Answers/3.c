#include <stdio.h>
#include <stdlib.h>

#include <unistd.h>  //_getch*/
#include <termios.h> //_getch*/

void print(char c);

char getch()
{
    char buf = 0;
    struct termios old = {0};
    fflush(stdout);
    if (tcgetattr(0, &old) < 0)
        perror("tcsetattr()");
    old.c_lflag &= ~ICANON;
    old.c_lflag &= ~ECHO;
    old.c_cc[VMIN] = 1;
    old.c_cc[VTIME] = 0;
    if (tcsetattr(0, TCSANOW, &old) < 0)
        perror("tcsetattr ICANON");
    if (read(0, &buf, 1) < 0)
        perror("read()");
    old.c_lflag |= ICANON;
    old.c_lflag |= ECHO;
    if (tcsetattr(0, TCSADRAIN, &old) < 0)
        perror("tcsetattr ~ICANON");
    return buf;
}

int main(void)
{
    char c = 0;

    printf(
        "Welcome!\n"
        "Press CTRL+C to stop the program.\n"
        "Enter any character and see what happens.\n");
    while (1)
    {
        print(c);
        c = getch();
        system("clear");
    }

    return 0;
}

void print(char c)
{
    if (c == 0)
    {
        return;
    }

    if (c == '!')
    {
        printf("\e[31m");
        return;
    }

    if (c == '@')
    {
        printf("\e[32m");
        return;
    }

    if (c == '#')
    {
        printf("\e[33m");
        return;
    }

    if (c == '$')
    {
        printf("\e[34m");
        return;
    }

    if (c == '%')
    {
        printf("\e[35m");
        return;
    }

    if (c == '^')
    {
        printf("\e[36m");
        return;
    }

    if (c == '&')
    {
        printf("\e[91m");
        return;
    }

    if (c == '*')
    {
        printf("\e[92m");
        return;
    }

    if (c == '(')
    {
        printf("\e[96m");
        return;
    }

    if (c == ')')
    {
        printf("\e[97m");
        return;
    }

    if (c == '0')
    {
        printf(

            "   ▄▄▄▄   \n"
            "  ██▀▀██  \n"
            " ██    ██ \n"
            " ██ ██ ██ \n"
            " ██    ██ \n"
            "  ██▄▄██  \n"
            "   ▀▀▀▀   \n"

        );
        return;
    }

    if (c == '1')
    {
        printf(

            "   ▄▄▄    \n"
            "  █▀██    \n"
            "    ██    \n"
            "    ██    \n"
            "    ██    \n"
            " ▄▄▄██▄▄▄ \n"
            " ▀▀▀▀▀▀▀▀ \n"

        );
        return;
    }

    if (c == '2')
    {
        printf(

            "  ▄▄▄▄▄   \n"
            " █▀▀▀▀██▄ \n"
            "       ██ \n"
            "     ▄█▀  \n"
            "   ▄█▀    \n"
            " ▄██▄▄▄▄▄ \n"
            " ▀▀▀▀▀▀▀▀ \n"

        );
        return;
    }

    if (c == '3')
    {
        printf(

            "  ▄▄▄▄▄   \n"
            " █▀▀▀▀██▄ \n"
            "      ▄██ \n"
            "   █████  \n"
            "      ▀██ \n"
            " █▄▄▄▄██▀ \n"
            "  ▀▀▀▀▀   \n"

        );
        return;
    }

    if (c == '4')
    {
        printf(

            "     ▄▄▄  \n"
            "    ▄███  \n"
            "   █▀ ██  \n"
            " ▄█▀  ██  \n"
            " ████████ \n"
            "      ██  \n"
            "      ▀▀  \n"

        );
        return;
    }

    if (c == '5')
    {
        printf(

            " ▄▄▄▄▄▄▄  \n"
            " ██▀▀▀▀▀  \n"
            " ██▄▄▄▄   \n"
            " █▀▀▀▀██▄ \n"
            "       ██ \n"
            " █▄▄▄▄██▀ \n"
            "  ▀▀▀▀▀   \n"

        );
        return;
    }

    if (c == '6')
    {
        printf(

            "   ▄▄▄▄   \n"
            "  ██▀▀▀█  \n"
            " ██ ▄▄▄   \n"
            " ███▀▀██▄ \n"
            " ██    ██ \n"
            " ▀██▄▄██▀ \n"
            "   ▀▀▀▀   \n"

        );
        return;
    }

    if (c == '7')
    {
        printf(

            " ▄▄▄▄▄▄▄▄ \n"
            " ▀▀▀▀▀███ \n"
            "     ▄██  \n"
            "     ██   \n"
            "    ██    \n"
            "   ██     \n"
            "  ▀▀      \n"

        );
        return;
    }

    if (c == '8')
    {
        printf(

            "   ▄▄▄▄   \n"
            " ▄██▀▀██▄ \n"
            " ██▄  ▄██ \n"
            "  ██████  \n"
            " ██▀  ▀██ \n"
            " ▀██▄▄██▀ \n"
            "   ▀▀▀▀   \n"

        );
        return;
    }

    if (c == '9')
    {
        printf(

            "   ▄▄▄▄   \n"
            " ▄██▀▀██▄ \n"
            " ██    ██ \n"
            " ▀██▄▄███ \n"
            "   ▀▀▀ ██ \n"
            "  █▄▄▄██  \n"
            "   ▀▀▀▀   \n"

        );
        return;
    }

    if (c == 'a')
    {
        printf(

            "  ▄█████▄ \n"
            "  ▀ ▄▄▄██ \n"
            " ▄██▀▀▀██ \n"
            " ██▄▄▄███ \n"
            "  ▀▀▀▀ ▀▀ \n"

        );
        return;
    }

    if (c == 'b')
    {
        printf(

            " ▄▄       \n"
            " ██       \n"
            " ██▄███▄  \n"
            " ██▀  ▀██ \n"
            " ██    ██ \n"
            " ███▄▄██▀ \n"
            " ▀▀ ▀▀▀   \n"

        );
        return;
    }

    if (c == 'c')
    {
        printf(

            "  ▄█████▄ \n"
            " ██▀    ▀ \n"
            " ██       \n"
            " ▀██▄▄▄▄█ \n"
            "   ▀▀▀▀▀  \n"

        );
        return;
    }

    if (c == 'd')
    {
        printf(

            "       ▄▄ \n"
            "       ██ \n"
            "  ▄███▄██ \n"
            " ██▀  ▀██ \n"
            " ██    ██ \n"
            " ▀██▄▄███ \n"
            "   ▀▀▀ ▀▀ \n"

        );
        return;
    }

    if (c == 'e')
    {
        printf(

            "  ▄████▄  \n"
            " ██▄▄▄▄██ \n"
            " ██▀▀▀▀▀▀ \n"
            " ▀██▄▄▄▄█ \n"
            "   ▀▀▀▀▀  \n"

        );
        return;
    }

    if (c == 'f')
    {
        printf(

            "    ▄▄▄▄  \n"
            "   ██▀▀▀  \n"
            " ███████  \n"
            "   ██     \n"
            "   ██     \n"
            "   ██     \n"
            "   ▀▀     \n"

        );
        return;
    }

    if (c == 'g')
    {
        printf(

            "  ▄███▄██ \n"
            " ██▀  ▀██ \n"
            " ██    ██ \n"
            " ▀██▄▄███ \n"
            "  ▄▀▀▀ ██ \n"
            "  ▀████▀▀ \n"

        );
        return;
    }

    if (c == 'h')
    {
        printf(

            " ▄▄       \n"
            " ██       \n"
            " ██▄████▄ \n"
            " ██▀   ██ \n"
            " ██    ██ \n"
            " ██    ██ \n"
            " ▀▀    ▀▀ \n"

        );
        return;
    }

    if (c == 'i')
    {
        printf(

            "    ██    \n"
            "    ▀▀    \n"
            "  ████    \n"
            "    ██    \n"
            "    ██    \n"
            " ▄▄▄██▄▄▄ \n"
            " ▀▀▀▀▀▀▀▀ \n"

        );
        return;
    }

    if (c == 'j')
    {
        printf(

            "    ██    \n"
            "    ▀▀    \n"
            "  ████    \n"
            "    ██    \n"
            "    ██    \n"
            "    ██    \n"
            "    ██    \n"
            " ████▀    \n"

        );
        return;
    }

    if (c == 'k')
    {
        printf(

            " ▄▄       \n"
            " ██       \n"
            " ██ ▄██▀  \n"
            " ██▄██    \n"
            " ██▀██▄   \n"
            " ██  ▀█▄  \n"
            " ▀▀   ▀▀▀ \n"

        );
        return;
    }

    if (c == 'l')
    {
        printf(

            " ▄▄▄▄     \n"
            " ▀▀██     \n"
            "   ██     \n"
            "   ██     \n"
            "   ██     \n"
            "   ██▄▄▄  \n"
            "    ▀▀▀▀  \n"

        );
        return;
    }

    if (c == 'm')
    {
        printf(

            " ████▄██▄ \n"
            " ██ ██ ██ \n"
            " ██ ██ ██ \n"
            " ██ ██ ██ \n"
            " ▀▀ ▀▀ ▀▀ \n"

        );
        return;
    }

    if (c == 'n')
    {
        printf(

            " ██▄████▄ \n"
            " ██▀   ██ \n"
            " ██    ██ \n"
            " ██    ██ \n"
            " ▀▀    ▀▀ \n"

        );
        return;
    }

    if (c == 'o')
    {
        printf(

            "  ▄████▄  \n"
            " ██▀  ▀██ \n"
            " ██    ██ \n"
            " ▀██▄▄██▀ \n"
            "   ▀▀▀▀   \n"
            "          \n"

        );
        return;
    }

    if (c == 'p')
    {
        printf(

            " ██▄███▄  \n"
            " ██▀  ▀██ \n"
            " ██    ██ \n"
            " ███▄▄██▀ \n"
            " ██ ▀▀▀   \n"
            " ██       \n"

        );
        return;
    }

    if (c == 'q')
    {
        printf(

            "  ▄███▄██ \n"
            " ██▀  ▀██ \n"
            " ██    ██ \n"
            " ▀██▄▄███ \n"
            "   ▀▀▀ ██ \n"
            "       ██ \n"

        );
        return;
    }

    if (c == 'r')
    {
        printf(

            "  ██▄████ \n"
            "  ██▀     \n"
            "  ██      \n"
            "  ██      \n"
            "  ▀▀      \n"

        );
        return;
    }

    if (c == 's')
    {
        printf(

            " ▄▄█████▄ \n"
            " ██▄▄▄▄ ▀ \n"
            "  ▀▀▀▀██▄ \n"
            " █▄▄▄▄▄██ \n"
            "  ▀▀▀▀▀▀  \n"

        );
        return;
    }

    if (c == 't')
    {
        printf(

            "   ██     \n"
            " ███████  \n"
            "   ██     \n"
            "   ██     \n"
            "   ██▄▄▄  \n"
            "    ▀▀▀▀  \n"

        );
        return;
    }

    if (c == 'u')
    {
        printf(

            " ██    ██ \n"
            " ██    ██ \n"
            " ██    ██ \n"
            " ██▄▄▄███ \n"
            "  ▀▀▀▀ ▀▀ \n"

        );
        return;
    }

    if (c == 'v')
    {
        printf(

            " ██▄  ▄██ \n"
            "  ██  ██  \n"
            "  ▀█▄▄█▀  \n"
            "   ████   \n"
            "    ▀▀    \n"

        );
        return;
    }

    if (c == 'w')
    {
        printf(

            "██      ██\n"
            "▀█  ██  █▀\n"
            " ██▄██▄██ \n"
            " ▀██  ██▀ \n"
            "  ▀▀  ▀▀  \n"

        );
        return;
    }

    if (c == 'x')
    {
        printf(

            " ▀██  ██▀ \n"
            "   ████   \n"
            "   ▄██▄   \n"
            "  ▄█▀▀█▄  \n"
            " ▀▀▀  ▀▀▀ \n"

        );
        return;
    }

    if (c == 'y')
    {
        printf(

            " ▀██  ███ \n"
            "  ██▄ ██  \n"
            "   ████▀  \n"
            "    ███   \n"
            "    ██    \n"
            "  ███     \n"

        );
        return;
    }

    if (c == 'z')
    {
        printf(

            " ████████ \n"
            "     ▄█▀  \n"
            "   ▄█▀    \n"
            " ▄██▄▄▄▄▄ \n"
            " ▀▀▀▀▀▀▀▀ \n"

        );
        return;
    }

    if (c == 'A')
    {
        printf(

            "    ▄▄    \n"
            "   ████   \n"
            "   ████   \n"
            "  ██  ██  \n"
            "  ██████  \n"
            " ▄██  ██▄ \n"
            " ▀▀    ▀▀ \n"

        );
        return;
    }

    if (c == 'B')
    {
        printf(

            " ▄▄▄▄▄▄   \n"
            " ██▀▀▀▀██ \n"
            " ██    ██ \n"
            " ███████  \n"
            " ██    ██ \n"
            " ██▄▄▄▄██ \n"
            " ▀▀▀▀▀▀▀  \n"

        );
        return;
    }

    if (c == 'C')
    {
        printf(

            "    ▄▄▄▄  \n"
            "  ██▀▀▀▀█ \n"
            " ██▀      \n"
            " ██       \n"
            " ██▄      \n"
            "  ██▄▄▄▄█ \n"
            "    ▀▀▀▀  \n"

        );
        return;
    }

    if (c == 'D')
    {
        printf(

            " ▄▄▄▄▄    \n"
            " ██▀▀▀██  \n"
            " ██    ██ \n"
            " ██    ██ \n"
            " ██    ██ \n"
            " ██▄▄▄██  \n"
            " ▀▀▀▀▀    \n"

        );
        return;
    }

    if (c == 'E')
    {
        printf(

            " ▄▄▄▄▄▄▄▄ \n"
            " ██▀▀▀▀▀▀ \n"
            " ██       \n"
            " ███████  \n"
            " ██       \n"
            " ██▄▄▄▄▄▄ \n"
            " ▀▀▀▀▀▀▀▀ \n"

        );
        return;
    }

    if (c == 'F')
    {
        printf(

            " ▄▄▄▄▄▄▄▄ \n"
            " ██▀▀▀▀▀▀ \n"
            " ██       \n"
            " ███████  \n"
            " ██       \n"
            " ██       \n"
            " ▀▀       \n"

        );
        return;
    }

    if (c == 'G')
    {
        printf(

            "    ▄▄▄▄  \n"
            "  ██▀▀▀▀█ \n"
            " ██       \n"
            " ██  ▄▄▄▄ \n"
            " ██  ▀▀██ \n"
            "  ██▄▄▄██ \n"
            "    ▀▀▀▀  \n"

        );
        return;
    }

    if (c == 'H')
    {
        printf(

            " ▄▄    ▄▄ \n"
            " ██    ██ \n"
            " ██    ██ \n"
            " ████████ \n"
            " ██    ██ \n"
            " ██    ██ \n"
            " ▀▀    ▀▀ \n"

        );
        return;
    }

    if (c == 'I')
    {
        printf(

            "  ▄▄▄▄▄▄  \n"
            "  ▀▀██▀▀  \n"
            "    ██    \n"
            "    ██    \n"
            "    ██    \n"
            "  ▄▄██▄▄  \n"
            "  ▀▀▀▀▀▀  \n"

        );
        return;
    }

    if (c == 'J')
    {
        printf(

            "    ▄▄▄▄▄ \n"
            "    ▀▀▀██ \n"
            "       ██ \n"
            "       ██ \n"
            "       ██ \n"
            " █▄▄▄▄▄██ \n"
            "  ▀▀▀▀▀   \n"

        );
        return;
    }

    if (c == 'K')
    {
        printf(

            " ▄▄   ▄▄▄ \n"
            " ██  ██▀  \n"
            " ██▄██    \n"
            " █████    \n"
            " ██  ██▄  \n"
            " ██   ██▄ \n"
            " ▀▀    ▀▀ \n"

        );
        return;
    }

    if (c == 'L')
    {
        printf(

            " ▄▄       \n"
            " ██       \n"
            " ██       \n"
            " ██       \n"
            " ██       \n"
            " ██▄▄▄▄▄▄ \n"
            " ▀▀▀▀▀▀▀▀ \n"

        );
        return;
    }

    if (c == 'M')
    {
        printf(

            " ▄▄▄  ▄▄▄ \n"
            " ███  ███ \n"
            " ████████ \n"
            " ██ ██ ██ \n"
            " ██ ▀▀ ██ \n"
            " ██    ██ \n"
            " ▀▀    ▀▀ \n"

        );
        return;
    }

    if (c == 'N')
    {
        printf(

            " ▄▄▄   ▄▄ \n"
            " ███   ██ \n"
            " ██▀█  ██ \n"
            " ██ ██ ██ \n"
            " ██  █▄██ \n"
            " ██   ███ \n"
            " ▀▀   ▀▀▀ \n"

        );
        return;
    }

    if (c == 'O')
    {
        printf(

            "   ▄▄▄▄   \n"
            "  ██▀▀██  \n"
            " ██    ██ \n"
            " ██    ██ \n"
            " ██    ██ \n"
            "  ██▄▄██  \n"
            "   ▀▀▀▀   \n"

        );
        return;
    }

    if (c == 'P')
    {
        printf(

            " ▄▄▄▄▄▄   \n"
            " ██▀▀▀▀█▄ \n"
            " ██    ██ \n"
            " ██████▀  \n"
            " ██       \n"
            " ██       \n"
            " ▀▀       \n"

        );
        return;
    }

    if (c == 'Q')
    {
        printf(

            "   ▄▄▄▄   \n"
            "  ██▀▀██  \n"
            " ██    ██ \n"
            " ██    ██ \n"
            " ██    ██ \n"
            "  ██▄▄██▀ \n"
            "   ▀▀▀██  \n"
            "       ▀  \n"

        );
        return;
    }

    if (c == 'R')
    {
        printf(

            "▄▄▄▄▄▄   \n"
            "██▀▀▀▀██ \n"
            "██    ██ \n"
            "███████  \n"
            "██  ▀██▄ \n"
            "██    ██ \n"
            "▀▀    ▀▀▀\n"

        );
        return;
    }

    if (c == 'S')
    {
        printf(

            "   ▄▄▄▄   \n"
            " ▄█▀▀▀▀█  \n"
            " ██▄      \n"
            "  ▀████▄  \n"
            "      ▀██ \n"
            " █▄▄▄▄▄█▀ \n"
            "  ▀▀▀▀▀   \n"

        );
        return;
    }

    if (c == 'T')
    {
        printf(

            " ▄▄▄▄▄▄▄▄ \n"
            " ▀▀▀██▀▀▀ \n"
            "    ██    \n"
            "    ██    \n"
            "    ██    \n"
            "    ██    \n"
            "    ▀▀    \n"

        );
        return;
    }

    if (c == 'U')
    {
        printf(

            " ▄▄    ▄▄ \n"
            " ██    ██ \n"
            " ██    ██ \n"
            " ██    ██ \n"
            " ██    ██ \n"
            " ▀██▄▄██▀ \n"
            "   ▀▀▀▀   \n"

        );
        return;
    }

    if (c == 'V')
    {
        printf(

            " ▄▄    ▄▄ \n"
            " ▀██  ██▀ \n"
            "  ██  ██  \n"
            "  ██  ██  \n"
            "   ████   \n"
            "   ████   \n"
            "   ▀▀▀▀   \n"

        );
        return;
    }

    if (c == 'W')
    {
        printf(

            "▄▄      ▄▄\n"
            "██      ██\n"
            "▀█▄ ██ ▄█▀\n"
            " ██ ██ ██ \n"
            " ███▀▀███ \n"
            " ███  ███ \n"
            " ▀▀▀  ▀▀▀ \n"

        );
        return;
    }

    if (c == 'X')
    {
        printf(

            " ▄▄▄  ▄▄▄ \n"
            "  ██▄▄██  \n"
            "   ████   \n"
            "    ██    \n"
            "   ████   \n"
            "  ██  ██  \n"
            " ▀▀▀  ▀▀▀ \n"

        );
        return;
    }

    if (c == 'Y')
    {
        printf(

            "▄▄▄    ▄▄▄\n"
            " ██▄  ▄██ \n"
            "  ██▄▄██  \n"
            "   ▀██▀   \n"
            "    ██    \n"
            "    ██    \n"
            "    ▀▀    \n"

        );
        return;
    }

    if (c == 'Z')
    {
        printf(
            " ▄▄▄▄▄▄▄▄ \n"
            " ▀▀▀▀▀███ \n"
            "     ██▀  \n"
            "   ▄██▀   \n"
            "  ▄██     \n"
            " ███▄▄▄▄▄ \n"
            " ▀▀▀▀▀▀▀▀ \n"

        );
        return;
    }
}
