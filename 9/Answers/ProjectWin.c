#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <stdbool.h>
#include <assert.h>
#include <time.h>

#define MAX_DIGIT '6'
#define MIN_DIGIT '1'

typedef enum
{
    EASY = '1',
    MEDIUM,
    HARD,
    CRAZY
} difficulty;

typedef enum
{
    EASY_ROUNDS = 20,
    MEDIUM_ROUNDS = 15,
    HARD_ROUNDS = 10,
    CRAZY_MIN_ROUNDS = 5,
    CRAZY_MAX_ROUNDS = 25,
} difficulyRounds;

char rollDigit();
void rollRandomCode();
bool isCodeRepeating(char a, char b, char c, char d);
void printMenu();
difficulty chooseOptionsMenu();
int getRounds(difficulty option);
void getUserGuess();
int getHits();
int getMisses();

// REMOVE AFTER
void wait();
void delayString(char string[], long delay);

// Global Variables
char comp_A = 0, comp_B = 0, comp_C = 0, comp_D = 0; // Random generated code
char user_A = 0, user_B = 0, user_C = 0, user_D = 0; // User's code

int main(void)
{
    srand(time(NULL));

    difficulty gameDifficulty = 0;
    int rounds = 0, currentRounds = 0;
    int hits = 0, misses = 0;
    bool hasWon = false;
    char playAgain = 0;

    do
    {
        gameDifficulty = chooseOptionsMenu();
        rounds = getRounds(gameDifficulty);

        rollRandomCode();

        do
        {
            if (gameDifficulty == CRAZY)
            {
                printf("C-R-A-Z-Y mode!\n");
            }
            else
            {
                printf("%d turns left!\n", rounds - currentRounds);
            }

            getUserGuess();

            hits = getHits();
            misses = getMisses();

            if (hits == 4)
            {
                hasWon = true;
            }
            else
            {
                ++currentRounds;
            }

            printf("You got:\n"
                   "\t%d HITS\n"
                   "\t%d MISSES\n",
                   hits, misses);
        } while (rounds > currentRounds && hasWon == false);

        if (hasWon)
        {
            printf("It took you only %d guesses, you are a professional code breaker!\n", currentRounds);
        }
        else
        {
            printf("OOOOHHHH!!! Pancratius won and bought all of Hanukkah's gifts.\n"
                   "Nothing left for you...\n"
                   "The secret password was %c%c%c%c!\n",
                   comp_A, comp_B, comp_C, comp_D);
        }

        do
        {
            printf("Would you like to play again? (y/n): ");
            playAgain = getch();
            printf("\n");
        } while (playAgain != 'y' && playAgain != 'n');
    } while (playAgain == 'y');

    delayString("\nBye bye!\n", 50000000L);

    return 0;
}

/*
? Checks if the given character is a valid character in the code
> Input: {char} c - the character to check
< Output: {char} randomCharacter - is the character in the correct range of [MIN_DIGIT, MAX_DIGIT]
*/
bool isValid(char c)
{
    return MIN_DIGIT <= c && c <= MAX_DIGIT;
}

/*
? Rolls a random valid digit
! Input: none
< Output: {char} randomDigit - the random digit that has been generated
*/
char rollDigit()
{
    char randomDigit = rand() % (MAX_DIGIT - MIN_DIGIT + 1) + MIN_DIGIT;
    assert(isValid(randomDigit));
    return randomDigit;
}

/*
? Rolls a random computer code
! Input: none
! Output: none
*/
void rollRandomCode()
{
    do
    {
        comp_A = rollDigit();
        comp_B = rollDigit();
        comp_C = rollDigit();
        comp_D = rollDigit();
    } while (isCodeRepeating(comp_A, comp_B, comp_C, comp_D));
}

/*
? Checks if the random generated code is valid (= does not contain repeating characters).
> Input: {char} a - the first random character of the generated code
> Input: {char} b - the second random character of the generated code
> Input: {char} c - the third random character of the generated code
> Input: {char} d - the fourth random character of the generated code
< Output: {bool} isValid - is the given code valid or not.
*/
bool isCodeRepeating(char a, char b, char c, char d)
{
    return a == b || a == c || a == d || b == c || b == d || c == d;
}

/*
? Prints the starting menu
! Input: none
! Output: none
*/
void printMenu()
{
    delayString("Welcome to \"", 50000000L);                  // .05s
    delayString("MAGSHIMIM CODE-BREAKER\"!!!\n", 300000000L); // .3s
    wait(2000000000L);                                        //2s
    delayString("A secret password was chosen to protect the credit card of Pancratius,\n"
                "the descendant of Antiochus.\n"
                "Your mission is to stop Pancratius by revealing his secret password.\n"
                "The rules are as follows:\n"
                "1. In each round you try to guess the secret password (4 distinct digits)\n"
                "2. After every guess you'll receive two hints about the password\n"
                "   HITS:   The number of digits in your guess which were exactly right.\n"
                "   MISSES: The number of digits in your guess which belongs to\n"
                "           the password but were miss-placed.\n"
                "3. If you'll fail to guess the password after a certain number of rounds\n"
                "   Pancratius will buy all the gifts for Hanukkah!!!\n\n",
                50000000L);                                    // .05s
    wait(2000000000L);                                         // 2s
    delayString("Please choose the game level:\n", 50000000L); // .05s
}

/*
? Gets the difficulty from the user
! Input: none
< Output: {difficulty} option - the difficulty the user had chosen
*/
difficulty chooseOptionsMenu()
{
    char option;

    //printMenu();

    do
    {
        printf("Which difficulty would like to play in?\n"
               "%c - Easy: %d rounds\n"
               "%c - Medium: %d rounds\n"
               "%c - Hard: %d rounds\n"
               "%c - Crazy: %d to %d rounds\n"
               "Make a choice: ",
               EASY, EASY_ROUNDS, MEDIUM, MEDIUM_ROUNDS, HARD, HARD_ROUNDS, CRAZY, CRAZY_MIN_ROUNDS, CRAZY_MAX_ROUNDS);

        scanf("%c", &option);
    } while (option < EASY || option > CRAZY);

    return (difficulty)option;
}

/*
? Gets the number of rounds for the game
> Input: {difficulty} option - the difficulty option for the game
< Output: {int} rounds - the number of rounds for the difficuly.
*/
int getRounds(difficulty option)
{
    int rounds = 0;

    switch (option)
    {
    case EASY:
        rounds = EASY_ROUNDS;
        break;
    case MEDIUM:
        rounds = MEDIUM_ROUNDS;
        break;
    case HARD:
        rounds = HARD_ROUNDS;
        break;
    case CRAZY:
        rounds = rand() % (CRAZY_MAX_ROUNDS - CRAZY_MIN_ROUNDS + 1) + CRAZY_MIN_ROUNDS;
        break;
    default:
        printf("Something went wrong...");
    }

    return rounds;
}

/*
? Gets the 4 code inputs from the user
! Input: none
! Output: none
*/
void getUserGuess()
{
    bool isCurrentValid = false;

    while (!isCurrentValid)
    {
        printf("Write your guess (only %c-%c, no ENTER is needed)\n", MIN_DIGIT, MAX_DIGIT);
        user_A = getch();
        user_B = getch();
        user_C = getch();
        user_D = getch();

        printf("\n");

        if (!isValid(user_A) || !isValid(user_A) || !isValid(user_A) || !isValid(user_A))
        {
            printf("Each number must be between %c and %c!\n", MIN_DIGIT, MAX_DIGIT);
        }
        else if (isCodeRepeating(user_A, user_B, user_C, user_D))
        {
            printf("All numbers must be different!\n");
        }
        else
        {
            isCurrentValid = true;
        }
    }
}

/*
? Calculates how many hits there where
? (hit = digit in user's code equals digit in computer's code)
! Input: none
< Output: {int} - the number of hits in the user's code
*/
int getHits()
{
    return (user_A == comp_A) + (user_B == comp_B) + (user_C == comp_C) + (user_D == comp_D);
}

/*
? Calculates how many missts there where
? (hit = digit in user's code is also in computer's code)
! Input: none
< Output: {int} - the number of misses in the user's code
*/
int getMisses()
{
    int misses = 0;
    misses += isCodeRepeating(user_A, comp_B, comp_C, comp_D);
    misses += isCodeRepeating(comp_A, user_B, comp_C, comp_D);
    misses += isCodeRepeating(comp_A, comp_B, user_C, comp_D);
    misses += isCodeRepeating(comp_A, comp_B, comp_C, user_D);
    return misses;
}

/***********************************************
*                                              *
* CODE FROM HERE IS JUST FOR PRINTING THE MENU *
*                                              *
************************************************/

// https://stackoverflow.com/a/36095407/10337548
long get_nanos(void)
{
    struct timespec ts;
    timespec_get(&ts, TIME_UTC);
    return (long)ts.tv_sec * 1000000000L + ts.tv_nsec;
}

void wait(long delay)
{
    long end, start = get_nanos();
    do
        end = get_nanos();
    while (end - start < delay);
}

/*
! Don't worry if you don't understand what I'm using here
? Prints the menu with a delay between letters
> Input: none
< Output: none
*/
void delayString(char string[], long delay)
{
    for (int i = 0; string[i]; i++)
    {
        wait(delay);
        printf("%c", string[i]);
        fflush(stdout);
    }
}
