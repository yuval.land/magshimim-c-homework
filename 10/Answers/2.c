#include <stdio.h>

#define LENGTH 5

void printReverse(int array[], int size);

int main(void)
{
    int array[LENGTH];
    int current = 0;

    printf("Enter the array: ");
    for (int i = 0; i < LENGTH; i++)
    {
        scanf("%d", &current);
        array[i] = current;
    }
    printReverse(array, LENGTH);

    return 0;
}

/*
? Prints a given array
> Input: {int[]} array - the array to print
> Input: {int} size - the size of the array
! Output: none
*/
void printReverse(int array[], int size)
{
    printf("After reverse: ");
    for (int i = size - 1; i >= 0; i--)
    {
        printf("%d ", array[i]);
    }
    printf("\n");
}