#include <stdio.h>

#define LUCKY_NUMBER_COUNT
#define NAME_LENGTH 14
#define HW_GRADES_COUNT 5
#define NUMBER_COUNT 10

void setArrayToValue(int array[], int size, int value);
void printIntArray(int array[], int size);

int main(void)
{
    float arrMyLuckyNumbers[LUCKY_NUMBER_COUNT] = {7, 13.0, .5};
    char arrMyName[NAME_LENGTH];
    int arrHWGrades[HW_GRADES_COUNT], currentGrade = 0;
    int arrNums[NUMBER_COUNT];

    arrMyName[0] = 'Y';
    arrMyName[1] = 'u';
    arrMyName[2] = 'v';
    arrMyName[3] = 'a';
    arrMyName[4] = 'l';
    arrMyName[5] = ' ';
    arrMyName[6] = 'M';
    arrMyName[7] = 'e';
    arrMyName[8] = 's';
    arrMyName[9] = 'h';
    arrMyName[10] = 'o';
    arrMyName[11] = 'r';
    arrMyName[12] = 'e';
    arrMyName[13] = 'r';

    for (int i = 0; i < HW_GRADES_COUNT; i++)
    {
        printf("Enter a grade: ");
        scanf("%d", &currentGrade);
        arrHWGrades[i] = currentGrade;
    }

    setArrayToValue(arrNums, NUMBER_COUNT, 5);

    printIntArray(arrHWGrades, HW_GRADES_COUNT);
    printIntArray(arrNums, NUMBER_COUNT);

    return 0;
}

/*
? Sets an array to be equal to a certain value
> Input: {int[]} array - the array to modify
> Input: {int} size - the size of the array
> Input: {int} value - the value to modify to
! Output: none
*/
void setArrayToValue(int array[], int size, int value)
{
    for (int i = 0; i < size; i++)
    {
        array[i] = value;
    }
}

/*
? Prints a given array
> Input: {int[]} array - the array to print
> Input: {int} size - the size of the array
! Output: none
*/
void printIntArray(int array[], int size)
{
    for (int i = 0; i < size; i++)
    {
        printf("%d", array[i]);

        if (i != size - 1) // if i is not the last value
        {
            printf(", ");    
        }
    }
    printf("\n");
}