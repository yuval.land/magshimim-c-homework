#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define ROLL_COUNT 10000
#define NUM_OF_FACES 6

int main(void)
{
    srand(time(NULL));

    int rolls[ROLL_COUNT], numberCount[NUM_OF_FACES] = {};
    int current = 0;

    for (int i = 0; i < ROLL_COUNT; i++)
    {
        current = rand() % NUM_OF_FACES + 1;
        rolls[i] = current;
        numberCount[current - 1] += 1;
    }

    for (int i = 0; i < NUM_OF_FACES; i++)
    {
        printf("%d: %d\n", i + 1, numberCount[i]);
    }

    return 0;
}