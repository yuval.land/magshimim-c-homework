/*********************************
* Class: MAGSHIMIM C1			 *
* Week 9           				 *
* MagshiParty					 *
**********************************/

#include <stdio.h>
#include <stdlib.h>

#define NUM_GUESTS 200
#define CHILD_AGE 12

#define FALSE 0
#define TRUE !FALSE

void sortArray(int ages[], int size);
int binarySearch(int ages[], int size, int age);

float averageAge(int ages[], int size);
int getChildCount(int ages[], int size);
int findAge(int ages[], int size, int age);
int findPair(int ages[], int size, int age);
void ageRange(int ages[], int size);

int main(void)
{
	int ageForSearch = 0, ageForPair = 0;
	
	int guestAges[NUM_GUESTS] = {42, 108, 95, 101, 90, 5, 79, 79, 83, 105, 66, 66, 2, 28, 2, 12, 116, 63, 28, 37,
								 112, 85, 63, 34, 53, 23, 22, 117, 39, 96, 48, 7, 12, 19, 70, 113, 108, 20, 116,
								 55, 24, 52, 3, 94, 34, 105, 22, 32, 54, 29, 108, 45, 23, 118, 118, 20, 84, 22,
								 50, 59, 77, 36, 111, 43, 49, 107, 41, 63, 65, 89, 87, 46, 51, 10, 11, 111, 7, 22,
								 34, 69, 70, 24, 85, 35, 37, 81, 47, 57, 12, 29, 25, 40, 27, 44, 18, 59, 39, 43,
								 10, 102, 34, 36, 80, 19, 25, 91, 100, 27, 114, 67, 102, 66, 45, 113, 31, 70, 18,
								 94, 58, 73, 107, 91, 42, 37, 36, 48, 16, 95, 72, 53, 111, 71, 22, 5, 47, 71, 28,
								 72, 8, 58, 98, 48, 34, 64, 66, 30, 50, 39, 102, 109, 63, 107, 27, 71, 94, 9,
								 61, 72, 43, 96, 11, 120, 25, 18, 69, 4, 116, 82, 3, 111, 92, 117, 15, 101, 37, 22,
								 109, 40, 109, 5, 2, 55, 54, 80, 19, 99, 61, 69, 8, 108, 9, 14, 49, 44, 48, 22,
								 31, 18, 14, 35};

	// Super useful!
	sortArray(guestAges, NUM_GUESTS);

	printf("Average age: %f\n", averageAge(guestAges, NUM_GUESTS));

	printf("Number of kids %d and under: %d\n", CHILD_AGE, getChildCount(guestAges, NUM_GUESTS));

	printf("Enter age to search: ");
	scanf("%d", &ageForSearch);

	if (findAge(guestAges, NUM_GUESTS, ageForSearch))
	{
		printf("Guest found!\n");
	}
	else
	{
		printf("No guest this age.\n");
	}

	printf("Enter age of guest looking for a friend: ");
	scanf("%d", &ageForPair);

	if (findPair(guestAges, NUM_GUESTS, ageForPair))
	{
		printf("A couple in the same age was found!\n");
	}
	else
	{
		printf("No couples found...\n");
	}

	ageRange(guestAges, NUM_GUESTS);

	return 0;
}

/*
? Sorts a given array from smallest to biggest
> Input: {int[]} ages - the array of guests' ages
> Input: {int} size - the size of the array `ages`
! Output: none
*/
void sortArray(int ages[], int size)
{
	int temp = 0;

	for (int i = 0; i < size - 1; i++)
	{
		for (int j = 0; j < size - i - 1; j++)
		{
			if (ages[j] > ages[j + 1])
			{
				temp = ages[j];
				ages[j] = ages[j + 1];
				ages[j + 1] = temp;
			}
		}
	}
}

/*
? Searches for a value using the "binary search" algorith
> Input: {int[]} ages - the array of guests' ages
> Input: {int} size - the size of the array `ages`
> Input: {int} age - the age to search for
< Output: {int} index - the index of the age, -1 if not found
*/
int binarySearch(int ages[], int size, int age)
{
	int index = -1;
	int middle = 0, lower = 0, upper = size - 2;

	while (lower <= upper && index == -1)
	{
		middle = lower + (upper - lower) / 2;
		if (ages[middle] == age)
		{
			index = middle;
		}
		else if (ages[middle] < age)
		{
			lower = middle + 1;
		}
		else
		{
			upper = middle - 1;
		}
	}

	return index;
}

/*
? Returns the average age of the guests in the party.
> Input: {int[]} ages - the array of guests' ages
> Input: {int} size - the size of the array `ages`
< Output: {float} avg - the average guest age
*/
float averageAge(int ages[], int size)
{
	float avg = 0;

	for (int i = 0; i < size; i++)
	{
		avg += ages[i];
	}
	avg /= size;

	return avg;
}

/*
? Returns the amount of children in the guest list
> Input: {int[]} ages - the array of guests' ages
> Input: {int} size - the size of the array `ages`
< Output: {int} i - the number of children in the guest list
*/
int getChildCount(int ages[], int size)
{
	/*
	Since the array is sorted, once we find an age that isn't a child
	we know that all the rest are not children either!
	*/

	int isStillChild = TRUE;
	int i = 0;

	for (i = 0; i < size && isStillChild; i++)
	{
		if (ages[i] > CHILD_AGE)
		{
			isStillChild = FALSE;
		}
	}

	return i - 1;
}

/*
? Checks if a given age is in the guest list
> Input: {int[]} ages - the array of guests' ages
> Input: {int} size - the size of the array `ages`
> Input: {int} age - the age to search for
< Output: {int} hasFound - was the age found in the guest list
*/
int findAge(int ages[], int size, int age)
{
	// Since the array is sorted, we can use binary-search!
	return binarySearch(ages, size, age) != -1;
}

/*
? Checks if a given age is in the guest list
> Input: {int[]} ages - the array of guests' ages
> Input: {int} size - the size of the array `ages`
> Input: {int} age - the age to search for
< Output: {int} hasFound - is there a pair of the same age
*/
int findPair(int ages[], int size, int age)
{
	/*
	All we need to do is find the index of the age in the sorted guest list
	and, since the array is sorted check the numbers above or below
	to see if they match the current number
	*/

	int index = binarySearch(ages, size, age);
	int hasFound = FALSE;

	if (index != -1)
	{
		if (index != 0 && ages[index + 1] == age)
		{
			hasFound = TRUE;
		}
		else if (index != size - 1 && ages[index - 1] == age)
		{
			hasFound = TRUE;
		}
	}

	return hasFound;
}

/*
? Prints the smallest and biggest age
> Input: {int[]} ages - the array of guests' ages
> Input: {int} size - the size of the array `ages`
! Output: none
*/
void ageRange(int ages[], int size)
{
	printf("Youngest guest age: %d\n", ages[0]);
	printf("Oldest guest age: %d\n", ages[size - 1]);
}