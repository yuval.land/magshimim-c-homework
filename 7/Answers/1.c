#include <stdio.h>

void printSentence();

int main(void)
{
    // We call the function `printSentence` 10 times.
    for (int i = 0; i < 10; i++)
    {
        // We call the function `printSentence`
        printSentence();
    }

    return 0;
}

/*
? Prints out the sentence.
! Input: none
! Output: none
*/
void printSentence()
{
    printf("My name is Yuval and I think functions are amazing!\n");
}
