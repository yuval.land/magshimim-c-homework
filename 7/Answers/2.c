#include <stdio.h>

void onesTriangle(int size);

int main(void)
{
    int size = 0;

    printf("Enter triangle size: ");
    scanf("%d", &size);

    // Calling `onesTriangle` with the size the user chose
    onesTriangle(size);

    return 0;
}

/*
? Prints a triangle of 1's.
* Input: {int} size - the size of the triangle (number of lines)
! Output: none
*/
void onesTriangle(int size)
{
    int triangle = 1, i = 0;

    for (i = 1; i <= size; i++)
    {
        printf("%d", triangle);
        triangle = triangle * 10 + 1; // 111 will turn into 1111
        printf("\n");
    }
}
