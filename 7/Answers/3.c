#include <stdio.h>

void addAndPrint(int a, int b);
void subAndPrint(int a, int b);

int main(void)
{
    int a = 0, b = 0;

    printf("Please enter two integers: ");
    scanf("%d %d", &a, &b);

    addAndPrint(a, b);
    subAndPrint(a, b);

    return 0;
}

/*
? Adds two given numbers and prints the result.
* Input: {int} a - the first number to add
* Input: {int} b - the second number to add
! Output: none
*/
void addAndPrint(int a, int b)
{
    printf("Result: %d\n", a + b);
}

/*
? Subtracts two given numbers and prints the result.
* Input: {int} a - the first number to subtract
* Input: {int} b - the second number to subtract
! Output: none
*/
void subAndPrint(int a, int b)
{
    // The reason we have `-b` is because "a + -b" == "a - b"
    addAndPrint(a, -b);
}
