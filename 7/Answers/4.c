#include <stdio.h>

void printNum(int num);

int main(void)
{
    int num = 6;
    // printNum(int num);
    /*
    The problem is that `int num` declares (מצהיר) a new variable
    instead of sending `num` to the `printNum` function.
    */
    printNum(num);
    return 0;
}

/*
? Prints a given number.
* Input: {int} num - number to print
! Output: none
*/
void printNum(int num)
{
    printf("%d", num);
}
