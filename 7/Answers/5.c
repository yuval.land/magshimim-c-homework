#include <stdio.h>
#include <math.h> // pow()
// When using math.h, be sure to compile like this:
// gcc -o 5.exe 5.c -lm

void printGeometricSeries(int a, int q, int n);

int main(void)
{
    int a = 0, q = 0, n = 0;
    
    printf("Enter first element of the series: ");
    scanf("%d", &a);
    printf("Enter the series ratio: ");
    scanf("%d", &q);
    printf("Enter number of elements to display: ");
    scanf("%d", &n);

    printGeometricSeries(a, q, n);

    return 0;
}

/*
? Calculates and prints a given gemoetric series
* Input: {int} a - the first element on the series (a0)
* Input: {int} q - the series` ratio (a1 = a0 * q)
* Input: {int} n - the number of elements in the series
! Output: none
*/
void printGeometricSeries(int a, int q, int n)
{
    int i = 0, currentElement = 0;

    for (i = 0; i < n; i++)
    {
        currentElement = a * pow(q, i);
        printf("%d ", currentElement);
    }
}
