#include <stdio.h>

#define MIN_PRIME 2
#define MAX_PRIME 1000

#define IS_PRIME 1
#define NOT_PRIME 0

int main(void)
{
	int i = 0, j = 0, primeCount = 0, prime = 0;

	for (i = MIN_PRIME; i < MAX_PRIME; i++)
	{
		/*
        We assume the number is a prime (prime = 1) and we run until we reach the end
        (the end being the number itself [j < i]) or until we know the number is not prime
        (&& prime)
        // Why is j initialized to 2?
        */
	    prime = IS_PRIME;
		
		for (j = MIN_PRIME; j < i && prime == IS_PRIME; j++)
		{
			// If we found a number (j) that divides the current number we are checking (i),
			// we know that number (i) is not a prime
			if (i % j == 0)
			{
				prime = NOT_PRIME;
			}
		}

		// If the number is a prime, we print it
		if (prime == IS_PRIME)
		{
			printf("%d is a Prime!\n", i);
			primeCount++;
		}
	}

	printf("We have found %d prime numbers!\n", primeCount);

	return 0;
}
