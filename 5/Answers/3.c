#include <stdio.h>

#define STARTING_VALUE 999
#define DECREMENT_VALUE 3

int main(void)
{
    int i = 0;

    // i starts at 999.
    // We run until i is 0 or less (not positive).
    // We decrement i by 3 each time (i = i - 3)
    for (i = STARTING_VALUE; i > 0; i -= DECREMENT_VALUE)
    {
        printf("%d, ", i);
    }

    printf("\n");

    return 0;
}
