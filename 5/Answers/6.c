#include <stdio.h>

int main(void)
{
	int myLoopyVar = 1;
	int multiplyBy = 0;
	
	printf("Daniel Presents: The Board of Multiplication!\n\n");
	
	// Main loop
	
	/* for(; myLoopyVar < 10; myLoopyVar++) */
	// Why isn't this correct?
	// Where is the initializer?
	for(myLoopyVar = 1; myLoopyVar < 10; myLoopyVar++)
	{	
		// Inner loop
		/* for(multiplyBy = 1; multiplyBy < 10; multiplyBy = multiplyBy++) */
		// What's wrong with the line above?
		// Remember PREFIX and POSTFIX!
		/*
			multiplyBy = multiplyBy++
		turns into:
			multipleBy = multiplyBy
			multiplyBy++ [[ multiplyBy = multiplyBy + 1 ]]
		*/
		// So how do we fix this? Remove `multiplyBy = `
		// Why?
		for(multiplyBy = 1; multiplyBy < 10; multiplyBy++)
			printf("%4d", myLoopyVar * multiplyBy); // Printing multiplication
		printf("\n"); // newline
	} // End of main loop
	
	// returning 0
	return 0;
}
