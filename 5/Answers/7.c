#include <stdio.h>

#define MAX 20
#define MIN 5

int main(void)
{
    int number = 0;
    int line = 0, star = 0;

    // Like in question 4, we input a number inside a given range.
    do
    {
        printf("Enter a number between 5 and 20: ");
        scanf("%d", &number);
        getchar();
    } while (number > MAX || number < MIN);

    // We need to print a total of n lines.
    for (line = 0; line < number; line++)
    {
        // We need to print x stars (*) for line x (for example, line 4 would have 4 stars)
        for (star = 0; star <= line; star++)
        {
            printf("*");
        }
        printf("\n");
    }

    return 0;
}
