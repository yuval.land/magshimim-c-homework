#include <stdio.h>

int main(void)
{
    int number = 0, copy = 0, reverse = 0;

    // Similar to Ex. 4, only this time the range is n<0
    do
    {
        printf("Enter a positive number: ");
        scanf("%d", &number);
        getchar();
    } while (number < 0);

    copy = number;

    /*
    How do we reverse the number?
    1. multiply `reverse` by 10.
    2. add the final digit of `number` (number % 10) to `reverse` // How?
    3. We divide `number` by 10 to "remove" it's final digit (123 / 10 = 12)
    */
    while (number > 0)
    {
        reverse *= 10;
        reverse += number % 10;
        number /= 10;
    }

    printf("Original number: %d\n", copy);
    printf("Reversed number: %d\n", reverse);

    return 0;
}
