#include <stdio.h>

#define MAX 100
#define MIN 1

int main(void)
{
    // We need to initialize i as a value that is not valid.
    // Why?
    int i = 0;

    while (i > MAX || i < MIN)
    {
        printf("Please enter a number between 1 and 100: ");
        scanf("%d", &i);
        getchar();
    }
    
    return 0;
}
