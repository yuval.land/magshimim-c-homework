#include <stdio.h>

#define MAX 100
#define MIN 1

int main(void)
{
    // Notice how this time we don't need to initialize i
    // because we change it's value [scanf] before using it [while (...)]
    // NOTE: Don't do this, it doesn't follow conventions (קונבנציות)
    int i;

    do
    {
        printf("Please enter a number between 1 and 100: ");
        scanf("%d", &i);
        getchar();
    } while (i > MAX || i < MIN);

    return 0;
}
