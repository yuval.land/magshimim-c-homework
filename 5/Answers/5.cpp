#include <stdio.h>

int main(void)
{
    /*
    int number: the number the user entered;
    int factorial: the factorial value of the number
    char next: the character the user entered to continue using the program.
    */
    int number = 0, factorial = 0;
    char next = 0;

    do
    {
        // Getting the number from the user
        printf("Enter the number to find it's factorial: ");
        scanf("%d", &number);
        getchar();

        printf("%d's factorial is: ", number);

        // Calculting the factorial
        // Notice how we use the `number` variable as our "loop variable"
        // and how we initialize `factorial` to 1
        for (factorial = 1; number > 0; number--)
        {
            factorial *= number;
        }

        printf("%d\n\n", factorial);

        // Gets the character from the user to see if the program should continue
        printf("Would you like to enter another number? ");
        next = getchar();
    } while (next == 'y' || next == 'Y');

    return 0;
}
