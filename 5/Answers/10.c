#include <stdio.h>

int main(void)
{
    int input = 0;

    printf("Please enter a number between 0 and 20: ");
    scanf("%d", &input);

    // 4 being the ending value
    while (input != 4)
    {
        switch (input)
        {
        /*
        A bonus is a bonus:
        What does `input = 4` do inside the printf?
        */
        case 0:
            printf("0 goes to %d!\n", input = 4);
            break;

        case 1:
            printf("1 goes to %d!\n", input = 3);
            break;

        case 2:
            printf("2 goes to %d!\n", input = 3);
            break;

        case 3:
            printf("3 goes to %d!\n", input = 5);
            break;

        case 4:
            // Will never run anyway. Why?
            break;

        case 5:
            printf("5 goes to %d!\n", input = 4);
            break;

        case 6:
            printf("6 goes to %d!\n", input = 3);
            break;

        case 7:
            printf("7 goes to %d!\n", input = 5);
            break;

        case 8:
            printf("8 goes to %d!\n", input = 5);
            break;

        case 9:
            printf("9 goes to %d!\n", input = 4);
            break;

        case 10:
            printf("10 goes to %d!\n", input = 3);
            break;

        case 11:
            printf("11 goes to %d!\n", input = 6);
            break;

        case 12:
            printf("12 goes to %d!\n", input = 5);
            break;

        case 13:
            printf("13 goes to %d!\n", input = 8);
            break;

        case 14:
            printf("14 goes to %d!\n", input = 8);
            break;

        case 15:
            printf("15 goes to %d!\n", input = 7);
            break;

        case 16:
            printf("16 goes to %d!\n", input = 7);
            break;

        case 17:
            printf("17 goes to %d!\n", input = 9);
            break;

        case 18:
            printf("18 goes to %d!\n", input = 9);
            break;

        case 19:
            printf("19 goes to %d!\n", input = 8);
            break;

        case 20:
            printf("20 goes to %d!\n", input = 6);
            break;
        }
    }

    printf("4 goes to MAGSHIMIM!\n");

    return 0;
}
